import math
n = int(input().strip())
string = input().strip()
nums_string = string.split(" ")
nums = [int(stri) for stri in nums_string]
nums.sort()

sum = 0;      
for i in range(n):
  sum += nums[i]
    
avg = (float(sum))/(float(n))
#print("avg= "+str(avg));

sq_sum = 0.0
for i in range(n):
  sq_sum += math.pow(nums[i]-avg, 2)

st_dev = round(math.sqrt(sq_sum/(float(n))), 1)
print(st_dev)


